//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/


import Foundation
import XCTest
@testable import NYC_Schools

final class SchoolDetailsViewModelTests: XCTestCase {

    func testViewModel() {
        let model: SchoolDetailsViewModel = SchoolDetailsViewModelImpl(schoolID: "01M292")
        
        let exp = XCTestExpectation()
        model.buildModel(onComplete: { result in
            switch result {
            case .success:
                XCTAssertEqual(model.pageTitle, "SAT Scores")
                XCTAssertEqual(model.writingTitle, "Writing Avg score:")
                XCTAssertEqual(model.satScores?.satWritingAvgScore, "363")
            case .failure:
                XCTFail("SchoolDetailsViewModel Tests Failed")
            }
            exp.fulfill()
        }, loadingBlock: { block in block() })
        wait(for: [exp], timeout: 0.1)
    }
}
