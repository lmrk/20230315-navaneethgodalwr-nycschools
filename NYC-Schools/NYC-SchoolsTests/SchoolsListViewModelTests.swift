//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/

import Foundation
import XCTest
@testable import NYC_Schools

final class SchoolsListViewModelTests: XCTestCase {
    
    func testViewModel() {
        let model: SchoolsListViewModel = SchoolsListViewModelImpl()
        
        let exp = XCTestExpectation()
        model.buildModel(onComplete: { result in
            switch result {
            case .success:
                XCTAssertEqual(model.pageTitle, "NYC Schools List")
                XCTAssertTrue(!model.schoolsList.isEmpty)
            case .failure:
                XCTFail("SchoolDetailsViewModel Tests Failed")
            }
            exp.fulfill()
        }, loadingBlock: { block in block() })
        wait(for: [exp], timeout: 0.1)
    }
}

