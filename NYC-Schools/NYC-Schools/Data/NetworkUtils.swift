//
/* 
 Created by navneet
 See LICENSE folder for licensing information.
 */

import Foundation

typealias LoadingBlock = (@escaping () -> Void) -> Void
typealias SuccessBlock = (Result<Void, AppError>) -> Void

struct AppError: Error {
    var code: Int = 500
    var message: String = "Internal server error"
}


func MockEntity<T: Decodable>(fileName: String) -> T? {
    let filePath = Bundle.main.path(
        forResource: fileName,
        ofType: ".json"
    )
    
    guard
        let path = filePath,
        let objData = try? Data(contentsOf: URL(fileURLWithPath: path))
    else { return nil }
    
    return T(data: objData)
}

extension Decodable {
    
    /// Creates a object conforming to decodable protocal from a data
    init?(data: Data?) {
        guard let data = data else { return nil }
        let object: Self
        do {
            object = try JSONDecoder().decode(Self.self, from: data)
        } catch (let e) {
            print(e)
            return nil
        }
        self = object
    }
}
