//
/* 
 Created by navneet
 See LICENSE folder for licensing information.
 */

import Foundation

// TODO: Reading from mock datastore - connect to network datstore
final class NYCRepositoryImpl: NYCRepository {
    func getNYCSchoolList(
        onComplete: @escaping SchoolsSuccessBlock,
        loadingBlock: LoadingBlock
    ) {
        loadingBlock {
            // TODO: inject filename and abstract
            if let schools: [NYCSchools] = MockEntity(fileName: "NYCSchools_Mock") {
                onComplete(.success(schools))
            } else {
                onComplete(.failure(AppError(message: "Parse error")))
            }
        }
    }
    
    func getSATScores(
        onComplete: @escaping SATScoresSuccessBlock,
        loadingBlock: LoadingBlock
    ) {
        loadingBlock {
            // TODO: inject filename and abstract
            if let satscores: [SATScores] = MockEntity(fileName: "SATScores_Mock") {
                onComplete(.success(satscores))
            } else {
                onComplete(.failure(AppError(message: "Parse error")))
            }
        }
    }
}
