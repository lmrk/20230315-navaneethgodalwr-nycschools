//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/


import UIKit

class SchoolsListViewController: UITableViewController {
    let viewModel: SchoolsListViewModel = SchoolsListViewModelImpl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadModel()
    }
    
    func loadModel() {
        viewModel.buildModel(
            onComplete: { result in
                switch result {
                case .success:
                    self.tableView.reloadData()
                case .failure:
                    print("error")
                }
            },
            loadingBlock: { block in
                //TODO: show custom data loading indicator if getting data from network
                block()
            }
        )
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schoolsList.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.pageTitle
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolNameCell", for: indexPath)

        let school = viewModel.schoolsList[indexPath.row]
        cell.textLabel?.text = school.schoolName
        cell.detailTextLabel?.text = school.primaryAddressLine1
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presentSchoolDetail(schoolId: viewModel.schoolsList[indexPath.row].dbn)
    }
    
    func presentSchoolDetail(schoolId: String) {
        let model = SchoolDetailsViewModelImpl(schoolID: schoolId)
        if let vc = SchoolDetailsVC.make(viewModel: model){
            vc.modalPresentationStyle = .pageSheet
            present(vc, animated: true)
        }
    }
}
