//
/* 
Created by navneet
See LICENSE folder for licensing information.
*/


import UIKit

class SchoolDetailsVC: UIViewController {
    @IBOutlet weak var closeBtn: UIStackView!
    @IBOutlet weak var mainStack: UIStackView!
    
    var viewModel: SchoolDetailsViewModel!
    static func make(viewModel: SchoolDetailsViewModel) -> SchoolDetailsVC? {
        let vc = UIStoryboard(name: "SchoolDetails", bundle: .main)
            .instantiateViewController(withIdentifier: String(describing: Self.self)) as? SchoolDetailsVC
        vc?.viewModel = viewModel
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadModel()
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func loadModel() {
        mainStack.clear()
        mainStack.addSpacer()
        let title = mainStack.addLabel(viewModel.pageTitle, .gray)
        title.font = .preferredFont(forTextStyle: .title2)
        mainStack.addSpacer()
        
        viewModel.buildModel(
            onComplete: { result in
                switch result {
                case .success:
                    self.buildViews()
                case .failure:
                    self.showError()
                }
            },
            loadingBlock: { block in
                //TODO: show custom data loading indicator if getting data from network
                block()
            }
        )
    }

    func showError() {
        mainStack.addLabel("SAT Scores Not found for this school", .red)
    }
    
    func buildViews() {
        guard let satScores = viewModel.satScores
        else {
            showError()
            return
        }
        let titleFont: UIFont = .preferredFont(forTextStyle: .body)
        let valueFont: UIFont = .preferredFont(forTextStyle: .headline)
        
        mainStack.addSpacer(40)
        mainStack.addLabel(viewModel.schoolNameTitle).font = .boldSystemFont(ofSize: 15)
        mainStack.addLabel(satScores.schoolName, .blue).font = valueFont
        mainStack.addSpacer(40)
        
        mainStack.addLabel(viewModel.satTakersTitle).font = titleFont
        mainStack.addLabel(satScores.numOfSatTestTakers).font = valueFont
        mainStack.addSpacer()
        
        mainStack.addLabel(viewModel.mathsTitle).font = titleFont
        mainStack.addLabel(satScores.satMathAvgScore).font = valueFont
        mainStack.addSpacer()
        
        mainStack.addLabel(viewModel.readingTitle).font = titleFont
        mainStack.addLabel(satScores.satCriticalReadingAvgScore).font = valueFont
        mainStack.addSpacer()
        
        mainStack.addLabel(viewModel.writingTitle).font = titleFont
        mainStack.addLabel(satScores.satWritingAvgScore).font = valueFont
        mainStack.addSpacer()
    }
}
