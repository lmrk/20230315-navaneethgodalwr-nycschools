//
//  ViewController.swift
//  NYC-Schools
//
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var mainStack: UIStackView!
    let viewModel: SchoolsListViewModel = SchoolsListViewModelImpl()
    
    
    var schoolBtns: [UIButton] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.pageTitle
        loadModel()
    }
    
    func loadModel() {
        mainStack.clear()
        mainStack.addSpacer()

        viewModel.buildModel(
            onComplete: { result in
                switch result {
                case .success:
                    self.buildViews()
                case .failure:
                    self.showError()
                }
            },
            loadingBlock: { block in
                //TODO: show custom data loading indicator
                block()
            }
        )
    }
    func showError() {
        mainStack.addLabel("Error loading", .red)
    }
    
    func buildViews() {
        guard viewModel.schoolsList.count > 0
        else {
            showError()
            return
        }

        viewModel.schoolsList.forEach({
            let school = SchoolItemView(schoolId: $0.dbn)
            school.decorate(schoolName: $0.schoolName)
            school.addTarget(self, action: #selector(onTapSchool), for: .touchUpInside)
            mainStack.addArrangedSubview(school)
            mainStack.addSpacer()
        })
    }

    @objc func onTapSchool(_ sender: UIButton) {
        guard let btn = sender as? SchoolItemView
        else { return }
        
        let model = SchoolDetailsViewModelImpl(schoolID: btn.schoolId)
        if let vc = SchoolDetailsVC.make(viewModel: model){
            vc.modalPresentationStyle = .pageSheet
            present(vc, animated: true)
        }
    }
}


final class SchoolItemView: UIButton {
    let schoolId: String
    
    init(frame: CGRect = .zero, schoolId: String) {
        self.schoolId = schoolId
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func decorate(schoolName: String) {
        setTitle(schoolName, for: .normal)
        titleLabel?.numberOfLines = 0
        backgroundColor = .clear
        setTitleColor(.black, for: .normal)
        isEnabled = true
        layer.cornerRadius = 10
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 0.5
        configuration = .borderless()
        configuration?.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)
    }
}
